﻿using System;

class NguoiLaoDong {
    public string HoTen;
    public int NamSinh;
    public double LuongCoBan;

    public NguoiLaoDong() {}
    public NguoiLaoDong(string hoten, int namsinh, double luongcoban) {
        HoTen = hoten;
        NamSinh = namsinh;
        LuongCoBan = luongcoban;
    }
    public void NhapThongTin(string hoten, int namsinh, double luongcoban) {
        HoTen = hoten;
        NamSinh = namsinh;
        LuongCoBan = luongcoban;
    }
    public virtual double TinhLuong() {
        return LuongCoBan;
    }
    public virtual void XuatThongTin() {
        Console.WriteLine("Ho ten la: {0}, nam sinh: {1}, luong co ban: {2}.", HoTen, NamSinh, LuongCoBan);
    }
}

class GiaoVien : NguoiLaoDong {
    public double HeSoLuong;

    public GiaoVien() {}
    public GiaoVien(string hoten, int namsinh, double luongcoban, double hesoluong) : base(hoten, namsinh, luongcoban) {
        HeSoLuong = hesoluong;
    }
    public void NhapThongTin(double hesoluong) {
        HeSoLuong = hesoluong;
    }
    public override double TinhLuong() {
        return LuongCoBan * HeSoLuong * 1.25;
    }
    public override void XuatThongTin() {
        base.XuatThongTin();
        Console.WriteLine("He so luong: {0}, Luong: {1}.", HeSoLuong, TinhLuong());
    }
    public virtual void XuLy() {
        HeSoLuong += 0.6;
    }
}

class Program {
    static void Main(string[] args) {
        Console.Write("So luong giao vien: ");
        int n;
        while (!Int32.TryParse(Console.ReadLine(), out n)){
            Console.WriteLine("So luong phai la so nguyen.");
            Console.Write("Nhap lai so luong giao vien: ");
        }

        while (n <= 0) {
            Console.WriteLine("So luong giao vien phai lon hon 0.");
            Console.Write("Nhap lai so luong giao vien: ");
            n = int.Parse(Console.ReadLine());
        }
        GiaoVien[] danhsachGV = new GiaoVien[n];

        for (int i = 0; i < n; i++) {
            Console.WriteLine("Thong tin giao vien thu {0}:", i + 1);
            Console.Write("Ho ten: ");
            string hoTen = Console.ReadLine();
            int namSinh;
            Console.Write("Nam sinh: ");
            while (!Int32.TryParse(Console.ReadLine(), out namSinh)){
                Console.WriteLine("Nam sinh phai la so");
                Console.Write("Nhap lai nam sinh: ");
            }
            
            Console.Write("Luong co ban: ");
            double luongCoBan;
            while (!double.TryParse(Console.ReadLine(), out luongCoBan)){
                Console.WriteLine("Luong co ban phai la so");
                Console.Write("Nhap lai luong co ban: ");
            }

            Console.Write("He so luong: ");
            double heSoLuong;
            while (!double.TryParse(Console.ReadLine(), out heSoLuong)){
                Console.WriteLine("He so luong phai la so");
                Console.Write("Nhap lai he so luong: ");
            }

            GiaoVien GV = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhsachGV[i] = GV;
        }

        double luongthapnhat = danhsachGV[0].TinhLuong();
        int count = 1;

        for (int i = 1; i < n; i++) {
            if (danhsachGV[i].TinhLuong() < luongthapnhat) {
                count = 1;
                luongthapnhat = danhsachGV[i].TinhLuong();
            }
            else if (danhsachGV[i].TinhLuong() == luongthapnhat) {
                count++;
            }
        }

        GiaoVien[] dsluongthapnhat = new GiaoVien[count];

        int j = 0;
        for (int i = 0; i < n; i++) {
            if (danhsachGV[i].TinhLuong() == luongthapnhat) {
                dsluongthapnhat[j] = danhsachGV[i];
                j++;
            }
        }

        if (count == 1) Console.WriteLine("Giao vien co luong thap nhat:");
        else Console.WriteLine("Co {0} giao vien co luong thap nhat:", count);

        for (int i = 0; i < count; i++) {
            dsluongthapnhat[i].XuatThongTin();
        }
        Console.Read();
    }
}